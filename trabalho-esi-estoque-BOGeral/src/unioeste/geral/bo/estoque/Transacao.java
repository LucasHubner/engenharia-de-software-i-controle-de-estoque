/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.estoque;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author lucas
 */
public class Transacao {
    int id;
    Date data;
    ArrayList<ProdutoTransacao> produtos;

    public ArrayList<ProdutoTransacao> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<ProdutoTransacao> produtos) {
        this.produtos = produtos;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

}
