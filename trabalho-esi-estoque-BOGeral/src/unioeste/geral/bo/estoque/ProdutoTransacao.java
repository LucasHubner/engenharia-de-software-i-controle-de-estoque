/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.estoque;

/**
 *
 * @author lucas
 */
public class ProdutoTransacao {
    Produto produto;
    float precoTransacao;
    int qtProduto;

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public float getPrecoTransacao() {
        return precoTransacao;
    }

    public void setPrecoTransacao(float precoTransacao) {
        this.precoTransacao = precoTransacao;
    }

    public int getQtProduto() {
        return qtProduto;
    }

    public void setQtProduto(int qtProduto) {
        this.qtProduto = qtProduto;
    }
    
    
}
