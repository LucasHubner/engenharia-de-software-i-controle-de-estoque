/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.estoque;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author lucas
 */
public class Produto implements Serializable {
    int codBarras;
    String nomeProduto;
    float precoCuso;
    float precoVenda;
    int qtdMinima;
    int qtdEstoque;
    ArrayList<TipoProduto> tiposProduto;

    public int getQtdEstoque() {
        return qtdEstoque;
    }

    public void setQtdEstoque(int qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    public ArrayList<TipoProduto> getTiposProduto() {
        return tiposProduto;
    }

    public void setTiposProduto(ArrayList<TipoProduto> tiposProduto) {
        this.tiposProduto = tiposProduto;
    }
    
    public int getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(int codBarras) {
        this.codBarras = codBarras;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public float getPrecoCuso() {
        return precoCuso;
    }

    public void setPrecoCuso(float precoCuso) {
        this.precoCuso = precoCuso;
    }

    public float getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(float precoVenda) {
        this.precoVenda = precoVenda;
    }

    public int getQtdMinima() {
        return qtdMinima;
    }

    public void setQtdMinima(int qtdMinima) {
        this.qtdMinima = qtdMinima;
    }
    
    
}
