/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.pessoa;

import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author lucas
 */
public class EnderecoEspecifico {
    int nroEndereco;
    String complementoEndereco = new String();
    Endereco endereco = new Endereco();

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }
    
    public int getNroEndereco() {
        return nroEndereco;
    }

    public void setNroEndereco(int nroEndereco) {
        this.nroEndereco = nroEndereco;
    }

    public String getComplementoEndereco() {
        return complementoEndereco;
    }

    public void setComplementoEndereco(String complementoEndereco) {
        this.complementoEndereco = complementoEndereco;
    }
    
    
    
}
