/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.pessoa;

/**
 *
 * @author lucas
 */
public class CNPJ {
    String cnpjString = new String();
    String cnpjFormatado = new String();

    public String getCnpjString() {
        return cnpjString;
    }

    public void setCnpjString(String cnpjString) {
        this.cnpjString = cnpjString;
    }

    public String getCnpjFormatado() {
        return cnpjFormatado;
    }

    public void setCnpjFormatado(String cnpjFormatado) {
        this.cnpjFormatado = cnpjFormatado;
    }
    
}
