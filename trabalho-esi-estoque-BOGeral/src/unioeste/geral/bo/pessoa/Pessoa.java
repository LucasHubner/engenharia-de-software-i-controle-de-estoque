/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.pessoa;

import java.util.ArrayList;
import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author lucas
 */
public class Pessoa{
    NomePessoa nome;
    EnderecoEspecifico enderecoPrincipal;
    ArrayList<Fone> telefones;

    public ArrayList<Fone> getTelefones() {
        return telefones;
    }

    public void setTelefones(ArrayList<Fone> telefones) {
        this.telefones = telefones;
    }
    
    public NomePessoa getNome() {
        return nome;
    }

    public void setNome(NomePessoa nome) {
        this.nome = nome;
    }

    public EnderecoEspecifico getEnderecoPrincipal() {
        return enderecoPrincipal;
    }

    public void setEnderecoPrincipal(EnderecoEspecifico enderecoPrincipal) {
        this.enderecoPrincipal = enderecoPrincipal;
    }
    
    
}
