/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.pessoa;

/**
 *
 * @author lucas
 */
public class TipoEnderecoVirtual {
    int idTipoEnderecoVirtual;
    String nomeTipoEnderecoVirtual;

    public int getIdTipoEnderecoVirtual() {
        return idTipoEnderecoVirtual;
    }

    public void setIdTipoEnderecoVirtual(int idTipoEnderecoVirtual) {
        this.idTipoEnderecoVirtual = idTipoEnderecoVirtual;
    }

    public String getNomeTipoEnderecoVirtual() {
        return nomeTipoEnderecoVirtual;
    }

    public void setNomeTipoEnderecoVirtual(String nomeTipoEnderecoVirtual) {
        this.nomeTipoEnderecoVirtual = nomeTipoEnderecoVirtual;
    }
    
    
}
