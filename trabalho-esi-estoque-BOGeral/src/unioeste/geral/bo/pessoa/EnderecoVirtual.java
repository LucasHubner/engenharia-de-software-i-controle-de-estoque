/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.bo.pessoa;

/**
 *
 * @author lucas
 */
public class EnderecoVirtual {
    String nomeEnderecoVirtual;
    TipoEnderecoVirtual tipoEnderecoVirtual;
    String endereco;

    public String getNomeEnderecoVirtual() {
        return nomeEnderecoVirtual;
    }

    public void setNomeEnderecoVirtual(String nomeEnderecoVirtual) {
        this.nomeEnderecoVirtual = nomeEnderecoVirtual;
    }

    public TipoEnderecoVirtual getTipoEnderecoVirtual() {
        return tipoEnderecoVirtual;
    }

    public void setTipoEnderecoVirtual(TipoEnderecoVirtual tipoEnderecoVirtual) {
        this.tipoEnderecoVirtual = tipoEnderecoVirtual;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    
}
