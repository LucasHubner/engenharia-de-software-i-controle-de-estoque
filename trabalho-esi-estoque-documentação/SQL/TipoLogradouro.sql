INSERT INTO `TipoLogradouro`(`idTipoLogradouro`, `siglaTipoLogradouro`, `nomeTipoLogradouro`) VALUES 
('1','AL','ALAMEDA'),
('2','AV','AVENIDA'),
('3','BC','BECO'),
('4','BL','BLOCO'),
('5','CAM','CAMINHO'),
('6','EST','ESTAÇÃO'),
('7','ETR','ESTRADA'),
('8','FAZ','FAZENDA'),
('9','GL','GALERIA'),
('10','LD','LADEIRA'),
('11','LGO','LARGO'),
('12','PÇA','PRAÇA'),
('13','PRQ','PARQUE'),
('14','PR','PRAIA'),
('15','QD','QUADRA'),
('16','ROD','RODOVIA'),
('17','R','RUA'),
('18','SQD','SUPER QUADRA'),
('19','TRV','TRAVESSA'),
('20','VD','VIADUTO')
;
