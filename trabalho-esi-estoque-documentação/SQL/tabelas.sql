SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`Produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Produto` (
  `codBarras` INT NOT NULL,
  `nomeProduto` VARCHAR(45) NULL,
  `precoCusto` FLOAT NULL,
  `precoVenda` FLOAT NULL,
  `qtdMinima` VARCHAR(45) NULL,
  PRIMARY KEY (`codBarras`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`TipoProduto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`TipoProduto` (
  `idTipoProduto` INT NOT NULL,
  `nomeTipoProduto` VARCHAR(45) NULL,
  PRIMARY KEY (`idTipoProduto`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`TipoLogradouro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`TipoLogradouro` (
  `idTipoLogradouro` INT NOT NULL AUTO_INCREMENT,
  `nomeTipoLogradouro` VARCHAR(45) NULL,
  `siglaTipoLogradouro` VARCHAR(45) NULL,
  PRIMARY KEY (`idTipoLogradouro`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Logradouro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Logradouro` (
  `idLogradouro` INT NOT NULL AUTO_INCREMENT,
  `nomeLogradouro` VARCHAR(45) NULL,
  `idTipoLogradouro` INT NOT NULL,
  PRIMARY KEY (`idLogradouro`, `idTipoLogradouro`),
  UNIQUE INDEX `idRua_UNIQUE` (`idLogradouro` ASC),
  INDEX `fk_Logradouro_TipoLogradouro1_idx` (`idTipoLogradouro` ASC),
  CONSTRAINT `fk_Logradouro_TipoLogradouro1`
    FOREIGN KEY (`idTipoLogradouro`)
    REFERENCES `mydb`.`TipoLogradouro` (`idTipoLogradouro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Bairro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Bairro` (
  `idBairro` INT NOT NULL AUTO_INCREMENT,
  `nomeBairro` VARCHAR(45) NULL,
  PRIMARY KEY (`idBairro`),
  UNIQUE INDEX `idBairro_UNIQUE` (`idBairro` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Pais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Pais` (
  `idPais` INT NOT NULL AUTO_INCREMENT,
  `nomePais` VARCHAR(45) NULL,
  `siglaPais` VARCHAR(45) NULL,
  PRIMARY KEY (`idPais`),
  UNIQUE INDEX `idPais_UNIQUE` (`idPais` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`UnidadeFederativa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`UnidadeFederativa` (
  `idUnidadeFederativa` INT NOT NULL AUTO_INCREMENT,
  `nomeUf` VARCHAR(45) NULL,
  `siglaUf` VARCHAR(45) NULL,
  PRIMARY KEY (`idUnidadeFederativa`),
  UNIQUE INDEX `idUnidadeFederativa_UNIQUE` (`idUnidadeFederativa` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cidade`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cidade` (
  `idCidade` INT NOT NULL AUTO_INCREMENT,
  `nomeCidade` VARCHAR(45) NULL,
  `idUnidadeFederativa` INT NULL,
  PRIMARY KEY (`idCidade`),
  UNIQUE INDEX `idCidade_UNIQUE` (`idCidade` ASC),
  INDEX `fk_Cidade_UnidadeFederativa_idx` (`idUnidadeFederativa` ASC),
  CONSTRAINT `fk_Cidade_UnidadeFederativa`
    FOREIGN KEY (`idUnidadeFederativa`)
    REFERENCES `mydb`.`UnidadeFederativa` (`idUnidadeFederativa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Endereco`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Endereco` (
  `idEndereco` INT NOT NULL AUTO_INCREMENT,
  `cep` INT NULL,
  `idRua` INT NOT NULL,
  `idBairro` INT NOT NULL,
  `idPais` INT NOT NULL,
  `idCidade` INT NOT NULL,
  PRIMARY KEY (`idEndereco`),
  UNIQUE INDEX `idEndereco_UNIQUE` (`idEndereco` ASC),
  INDEX `fk_Endereco_Rua1_idx` (`idRua` ASC),
  INDEX `fk_Endereco_Bairro1_idx` (`idBairro` ASC),
  INDEX `fk_Endereco_Pais1_idx` (`idPais` ASC),
  INDEX `fk_Endereco_Cidade1_idx` (`idCidade` ASC),
  CONSTRAINT `fk_Endereco_Rua1`
    FOREIGN KEY (`idRua`)
    REFERENCES `mydb`.`Logradouro` (`idLogradouro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Endereco_Bairro1`
    FOREIGN KEY (`idBairro`)
    REFERENCES `mydb`.`Bairro` (`idBairro`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Endereco_Pais1`
    FOREIGN KEY (`idPais`)
    REFERENCES `mydb`.`Pais` (`idPais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Endereco_Cidade1`
    FOREIGN KEY (`idCidade`)
    REFERENCES `mydb`.`Cidade` (`idCidade`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Cliente`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Cliente` (
  `idCliente` INT NOT NULL,
  `nomeCliente` VARCHAR(45) NULL,
  `cpfCliente` VARCHAR(45) NULL,
  `cnpjCliente` VARCHAR(45) NULL,
  `enderecoComplemento` VARCHAR(45) NULL,
  `enderecoNumero` INT NULL,
  `idEndereco` INT NOT NULL,
  PRIMARY KEY (`idCliente`),
  INDEX `fk_Cliente_Endereco1_idx` (`idEndereco` ASC),
  CONSTRAINT `fk_Cliente_Endereco1`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `mydb`.`Endereco` (`idEndereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Fornecedor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Fornecedor` (
  `cnpjFornecedor` INT NOT NULL,
  `nomeFornecedor` VARCHAR(45) NULL,
  `enderecoComplemento` VARCHAR(45) NULL,
  `enderecoNumero` INT NULL,
  `idEndereco` INT NOT NULL,
  PRIMARY KEY (`cnpjFornecedor`),
  INDEX `fk_Fornecedor_Endereco1_idx` (`idEndereco` ASC),
  CONSTRAINT `fk_Fornecedor_Endereco1`
    FOREIGN KEY (`idEndereco`)
    REFERENCES `mydb`.`Endereco` (`idEndereco`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Produto_has_TipoProduto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Produto_has_TipoProduto` (
  `codBarrasProduto` INT NOT NULL,
  `idTipoProduto` INT NOT NULL,
  PRIMARY KEY (`codBarrasProduto`, `idTipoProduto`),
  INDEX `fk_Produto_has_TipoProduto_TipoProduto1_idx` (`idTipoProduto` ASC),
  INDEX `fk_Produto_has_TipoProduto_Produto1_idx` (`codBarrasProduto` ASC),
  CONSTRAINT `fk_Produto_has_TipoProduto_Produto1`
    FOREIGN KEY (`codBarrasProduto`)
    REFERENCES `mydb`.`Produto` (`codBarras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Produto_has_TipoProduto_TipoProduto1`
    FOREIGN KEY (`idTipoProduto`)
    REFERENCES `mydb`.`TipoProduto` (`idTipoProduto`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Compra`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Compra` (
  `idCompra` INT NOT NULL,
  `dataCompra` DATE NULL,
  `cnpjFornecedor` INT NOT NULL,
  PRIMARY KEY (`idCompra`),
  INDEX `fk_Compra_Fornecedor1_idx` (`cnpjFornecedor` ASC),
  CONSTRAINT `fk_Compra_Fornecedor1`
    FOREIGN KEY (`cnpjFornecedor`)
    REFERENCES `mydb`.`Fornecedor` (`cnpjFornecedor`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Compra_has_Produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Compra_has_Produto` (
  `idCompra` INT NOT NULL,
  `codBarrasProduto` INT NOT NULL,
  `precoCompra` FLOAT NULL,
  PRIMARY KEY (`idCompra`, `codBarrasProduto`),
  INDEX `fk_Compra_has_Produto_Produto1_idx` (`codBarrasProduto` ASC),
  INDEX `fk_Compra_has_Produto_Compra1_idx` (`idCompra` ASC),
  CONSTRAINT `fk_Compra_has_Produto_Compra1`
    FOREIGN KEY (`idCompra`)
    REFERENCES `mydb`.`Compra` (`idCompra`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Compra_has_Produto_Produto1`
    FOREIGN KEY (`codBarrasProduto`)
    REFERENCES `mydb`.`Produto` (`codBarras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Venda`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Venda` (
  `idVenda` INT NOT NULL,
  `dataVenda` DATE NULL,
  `idCliente` INT NOT NULL,
  PRIMARY KEY (`idVenda`),
  INDEX `fk_Venda_Cliente1_idx` (`idCliente` ASC),
  CONSTRAINT `fk_Venda_Cliente1`
    FOREIGN KEY (`idCliente`)
    REFERENCES `mydb`.`Cliente` (`idCliente`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`Venda_has_Produto`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Venda_has_Produto` (
  `idVenda` INT NOT NULL,
  `codBarrasProduto` INT NOT NULL,
  `precoVenda` FLOAT NULL,
  PRIMARY KEY (`idVenda`, `codBarrasProduto`),
  INDEX `fk_Venda_has_Produto_Produto1_idx` (`codBarrasProduto` ASC),
  INDEX `fk_Venda_has_Produto_Venda1_idx` (`idVenda` ASC),
  CONSTRAINT `fk_Venda_has_Produto_Venda1`
    FOREIGN KEY (`idVenda`)
    REFERENCES `mydb`.`Venda` (`idVenda`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Venda_has_Produto_Produto1`
    FOREIGN KEY (`codBarrasProduto`)
    REFERENCES `mydb`.`Produto` (`codBarras`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
