package unioeste.geral.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEndereco.ColBairro;
import unioeste.geral.ColEndereco.ColCidade;
import unioeste.geral.ColEndereco.ColEndereco;
import unioeste.geral.ColEndereco.ColLogradouro;
import unioeste.geral.ColEndereco.ColPais;
import unioeste.geral.ColEndereco.ColTipoLogradouro;
import unioeste.geral.ColEndereco.ColUnidadeFederativa;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class UCEnderecoGeralServicos {
    private Conexao connection = new Conexao();

    public Logradouro cadastraLogradouro(Logradouro logradouro) throws SQLException{
        connection.AbreConexao();
        ColLogradouro colLogradouro = new ColLogradouro(connection);
        colLogradouro.Create(logradouro);
        connection.FechaConexao();
        return logradouro;
    }
    
    public Bairro cadastrarBairro(Bairro bairro) throws SQLException{
        connection.AbreConexao();
        ColBairro colBairro = new ColBairro(connection);
        colBairro.Create(bairro);
        connection.FechaConexao();
        return bairro;
    }
    public Endereco cadastrarEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        ColEndereco colEndereco = new ColEndereco(connection);
        colEndereco.Create(endereco);
        connection.FechaConexao();
        return endereco;
    }
    
    public boolean alterarEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        ColEndereco colEndereco = new ColEndereco(connection);
        colEndereco.Update(endereco);
        connection.FechaConexao();
        return false;
    }
    
    public boolean excluirEndereco(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        ColEndereco colEndereco = new ColEndereco(connection);
        colEndereco.Remove(endereco);
        connection.FechaConexao();
        return false;
    }
    
    public ArrayList<Endereco> ObterEnderecoPorCEP(Cep cep) throws SQLException{
        connection.AbreConexao();        
        ColEndereco colEndereco = new ColEndereco(connection);
        ArrayList<Endereco> enderecos = colEndereco.SearchByCep(cep);
        connection.FechaConexao();
        return enderecos;
    }
    
    public Endereco obterEnderecoPorID(Endereco endereco) throws SQLException{
        connection.AbreConexao();
        ColEndereco colEndereco = new ColEndereco(connection);                
        endereco = colEndereco.Read(endereco.getIdEndereco());
        connection.FechaConexao();
        return endereco;
    }
        
    public Cidade obterCidade(Cidade cidade) throws SQLException{
        connection.AbreConexao();
        ColCidade colCidade = new ColCidade(connection);
        cidade = colCidade.Read(cidade.getIdCidade());
        connection.FechaConexao();
        return cidade;
    }
    
    public HashMap<String, Integer> listTiposLogradouro(){
        HashMap<String,Integer> tiposLogradouro = new HashMap<>();
        
        if(connection.AbreConexao()){
            try {
                ArrayList<TipoLogradouro> tipoLogradouro;
                ColTipoLogradouro colTipoLogradouro = new ColTipoLogradouro(connection);
                tipoLogradouro = colTipoLogradouro.List();

                for(TipoLogradouro tipo:tipoLogradouro){
                    tiposLogradouro.put( tipo.getNomeTipoLogradouro(), tipo.getIdTipoLogradouro());
                }

            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }

            connection.FechaConexao();
        }
        return tiposLogradouro;
    }
    
    public HashMap<String, Integer> listEstados(){
        HashMap<String,Integer> estados = new HashMap<>();
        if(connection.AbreConexao()){
            try {
                ColUnidadeFederativa colUF = new ColUnidadeFederativa(connection);
                ArrayList<UnidadeFederativa> estadosArray = colUF.List();

                for(UnidadeFederativa estado: estadosArray ){
                    estados.put(estado.getSiglaUnidadeFederativa(), estado.getIdUnidadeFederativa());
                }

            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }

            connection.FechaConexao();
        }
        return estados;
    }
    public HashMap<String,Integer> listCidades(UnidadeFederativa uf){
        HashMap<String, Integer> cidades = new HashMap<>();
        if(connection.AbreConexao()){
            try{
                ColCidade colCidade = new ColCidade(connection);
                ArrayList<Cidade> cidadesArrayList = colCidade.ListByUnidadeFederativa(uf);
                for(Cidade cidade:cidadesArrayList){
                    cidades.put(cidade.getNomeCidade(), cidade.getIdCidade());
                }
            }
            catch(SQLException ex){
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            connection.FechaConexao();
        }
        return cidades;
    }
    
    public HashMap<String,Integer> listPaises(){
        HashMap<String, Integer> paises = new HashMap<>();
        if(connection.AbreConexao()){
            try {
                ColPais colPais = new ColPais(connection);
                ArrayList<Pais> paisesArrayList;
                paisesArrayList = colPais.List();
                for(Pais pais:paisesArrayList){
                paises.put(pais.getNomePais(), pais.getIdPais());
            }
            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            connection.FechaConexao();
            
        }
        return paises;
    }
    
    public HashMap<String, Integer> listBairros(){
        HashMap<String, Integer> bairros = new HashMap<>();
        if(connection.AbreConexao()){
            try {
                ColBairro colBairro = new ColBairro(connection);
                ArrayList<Bairro> bairrosArrayList;
                bairrosArrayList = colBairro.List();
                for(Bairro bairro:bairrosArrayList){
                bairros.put(bairro.getNomeBairro(), bairro.getIdBairro());
            }
            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            connection.FechaConexao();
        }
        return bairros;
    }
    
    public ArrayList<Bairro> listBairrosArrayList(){
        ArrayList<Bairro> bairrosList = new ArrayList<>();
        if(connection.AbreConexao()){
            try {
                ColBairro colBairro = new ColBairro(connection);
                bairrosList = colBairro.List();
                
            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            connection.FechaConexao();
        }
        
        return bairrosList;
    }
    
    public HashMap<String, Integer> listLogradouros(int idTipoLogradouro){
       HashMap<String, Integer> logradouros = new HashMap<>();
        if(connection.AbreConexao()){
            try {
                ColLogradouro colLogradouro = new ColLogradouro(connection);
                ArrayList<Logradouro> logradourosArrayList;
                logradourosArrayList = colLogradouro.ListByTipoLogradouro(idTipoLogradouro);
                for(Logradouro logradouro:logradourosArrayList){
                    logradouros.put(logradouro.getNomeLogradouro(),logradouro.getIdLogradouro());
                }
            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            connection.FechaConexao();
        }
        return logradouros;
    }

    public ArrayList<Logradouro> listLogradourosArrayList(Integer idTipoLogradouro) {
        ArrayList<Logradouro> logradouros = new ArrayList<>();
         if(connection.AbreConexao()){
            try {
                ColLogradouro colLogradouro = new ColLogradouro(connection);
                logradouros = colLogradouro.ListByTipoLogradouro(idTipoLogradouro);
            } catch (SQLException ex) {
                Logger.getLogger(UCEnderecoGeralServicos.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            connection.FechaConexao();
        }
         
        return logradouros;
    }
}
