/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColFornecedor.ColFornecedor;
import unioeste.geral.bo.pessoa.Fornecedor;

/**
 *
 * @author lucas
 */
public class UCFornecedorGeralServicos {
    Conexao conexao = new Conexao();
    
    public Fornecedor CadastraFornecedor(Fornecedor fornecedor) throws SQLException{
        conexao.AbreConexao();
        ColFornecedor col = new ColFornecedor(conexao);
        col.Create(fornecedor);
        conexao.FechaConexao();
        return fornecedor;
    }
    
    public ArrayList<Fornecedor> List() throws SQLException{
        ArrayList<Fornecedor> fornecedores = new ArrayList<>();
        conexao.AbreConexao();
        ColFornecedor col = new ColFornecedor(conexao);
        fornecedores = col.List();
        conexao.FechaConexao();
        
        return fornecedores;
    }
}
