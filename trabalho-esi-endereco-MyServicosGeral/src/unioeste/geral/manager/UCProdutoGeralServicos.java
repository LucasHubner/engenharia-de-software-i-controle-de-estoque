/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEstoque.ColProduto;
import unioeste.geral.bo.estoque.Produto;
import unioeste.geral.bo.estoque.TipoProduto;

/**
 *
 * @author lucas
 */
public class UCProdutoGeralServicos {
    Conexao conexao = new Conexao();
    
    public Produto CadastraProduto(Produto produto) throws SQLException{
        conexao.AbreConexao();
        ColProduto colProduto = new ColProduto(conexao);
        colProduto.Create(produto);
        conexao.FechaConexao();
        
        return produto;
    }
    
    public ArrayList<Produto> ListaProdutos() throws SQLException{
        ArrayList<Produto> produtos = new ArrayList<>();
        
        conexao.AbreConexao();
        ColProduto colProduto = new ColProduto(conexao);
        produtos = colProduto.List();
        conexao.FechaConexao();
        
        return produtos;
    }
    
    public ArrayList<TipoProduto> ListaTiposProduto(){
        ArrayList<TipoProduto> tipos = new ArrayList<>();
        
        return tipos;
    }
}
