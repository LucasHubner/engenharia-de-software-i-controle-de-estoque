/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.manager;

import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColCliente.ColCliente;
import unioeste.geral.bo.pessoa.Cliente;

/**
 *
 * @author lucas
 */
public class UCClienteGeralServicos {
    Conexao connection = new Conexao();
    
    public Cliente CadastrarCliente(Cliente cliente) throws SQLException{
        connection.AbreConexao();
        ColCliente colCliente = new ColCliente(connection);
        colCliente.Create(cliente);
        connection.FechaConexao();
        return cliente;
    }
    
    public ArrayList<Cliente> ListaClientes() throws SQLException{
        connection.AbreConexao();
        ColCliente colCliente = new ColCliente(connection);
        ArrayList<Cliente> clientes = colCliente.List();
        connection.FechaConexao();
        
        return clientes;
    }
    
}
