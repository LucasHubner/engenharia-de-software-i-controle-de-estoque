/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.manager;

import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColCompra.ColCompra;
import unioeste.geral.bo.estoque.Compra;

/**
 *
 * @author lucas
 */
public class UCCompraGeralServicos {
    Conexao conexao = new Conexao();
    
    public Compra CadastraCompra(Compra compra) throws Exception{
        try{
            conexao.AbreConexao();
            ColCompra colCompra = new ColCompra(conexao);
            compra = colCompra.Create(compra);
            conexao.FechaConexao();
            return compra;
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());        
            throw ex;
        }
    }
    
}
