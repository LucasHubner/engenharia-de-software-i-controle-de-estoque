/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.ColFornecedor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEndereco.ColEndereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.CPF;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.NomePessoa;

/**
 *
 * @author lucas
 */
public class ColFornecedor {
    Conexao conexao;

    public ColFornecedor(Conexao conexao) {
        this.conexao = conexao;
    }
    
     private Fornecedor PopulaFornecedor(ResultSet resultado) throws SQLException{
        Fornecedor fornecedor = new Fornecedor();
        //CNPJ
        CNPJ cnpj = new CNPJ();
        cnpj.setCnpjString(resultado.getString("cnpjFornecedor"));
        fornecedor.setCnpj(cnpj);
        
        //Nome
        NomePessoa nome = new NomePessoa();
        nome.setNomeCompleto(resultado.getString("nomeFornecedor"));
        fornecedor.setNome(nome);
        
        //Endereco Complemento
        EnderecoEspecifico enderecoEsp = new EnderecoEspecifico();
        enderecoEsp.setComplementoEndereco(resultado.getString("enderecoComplemento"));
        enderecoEsp.setNroEndereco(resultado.getInt("enderecoNumero"));
        ColEndereco colEndereco = new ColEndereco(conexao);
        enderecoEsp.setEndereco(colEndereco.Read(resultado.getInt("idEndereco")));
        fornecedor.setEnderecoPrincipal(enderecoEsp);
        
                
        return fornecedor;
    }
    
    public Fornecedor Create(Fornecedor cliente) throws SQLException{
        conexao.setDeclaracao("INSERT INTO `Fornecedor`(`cnpjFornecedor`, "
                                                + "`nomeFornecedor`, "
                                                + "`enderecoComplemento`, "
                                                + "`enderecoNumero`, "
                                                + "`idEndereco`)"
                + " VALUES (?,?,?,?,?)");
        
        
        conexao.getDeclaracao().setString(1,cliente.getCnpj().getCnpjString());
        conexao.getDeclaracao().setString(2, cliente.getNome().getNomeCompleto());        
        conexao.getDeclaracao().setString(3,cliente.getEnderecoPrincipal().getComplementoEndereco());
        conexao.getDeclaracao().setInt(4,cliente.getEnderecoPrincipal().getNroEndereco());
        conexao.getDeclaracao().setInt(5,cliente.getEnderecoPrincipal().getEndereco().getIdEndereco());
        
        conexao.getDeclaracao().executeUpdate();
        
        return cliente;
    }
    
    public ArrayList<Fornecedor> List() throws SQLException{
        ArrayList<Fornecedor> fornecedores = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Fornecedor");
        conexao.executeQueryAndSetDeclaracao();
        while(conexao.getResultado().next()){
            fornecedores.add(PopulaFornecedor(conexao.getResultado()));
        }
        
        return fornecedores;
    }
}
