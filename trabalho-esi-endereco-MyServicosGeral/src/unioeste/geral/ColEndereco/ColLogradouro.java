package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.TipoLogradouro;

/**
 *
 * @author lucas
 */
public class ColLogradouro {
    
    private Conexao conexao;

    public ColLogradouro(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Logradouro PopulaLogradouro(ResultSet resultado) throws SQLException{
        Logradouro logradouro = new Logradouro();
        
        logradouro.setIdLogradouro(resultado.getInt("idLogradouro"));
        logradouro.setNomeLogradouro(resultado.getString("nomeLogradouro"));
        ColTipoLogradouro colTipoLogradouro = new ColTipoLogradouro(conexao);
        logradouro.setTipoLogradouro(colTipoLogradouro.Read(resultado.getInt("idTipoLogradouro")));

        return logradouro;
    }
    
    
    public Boolean Create(Logradouro logradouro) throws SQLException{
        
        conexao.setDeclaracao("INSERT INTO `Logradouro` (`idLogradouro`, `nomeLogradouro`, `idTipoLogradouro`) "
                + "VALUES (NULL, '"+logradouro.getNomeLogradouro()+"', '"+logradouro.getTipoLogradouro().getIdTipoLogradouro()+"')");
        /*
            Atributos da tabela logradouro
                                -int idLogradouro -> Como o ID é AUTO INCREMENT, deixa null
                                -string nomeLogradouro
                                -int idTipoLogradouro
        */
      
        //Seta o nome logradouro
        //conexao.getDeclaracao().setString(1, logradouro.getNomeLogradouro());
        //Seta o id do TipoLogradouro, observe que a ID esta dentro do objeto TipoLogradouro
        // para isso, acessamos o objeto base Logradouro e depois pedimos ao metodo
        //getTipoLogradouro, com isso temos o objeto TipoLogradouro, e utilizamos seu metodo getIdTipoLogradouro
        //conexao.getDeclaracao().setInt(2, logradouro.getTipoLogradouro().getIdTipoLogradouro());
        
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Logradouro Read(int idLogradouro) throws SQLException {
        
        // SELECIONE TUDO DE logradouro ONDE o idLogradouro for IGUAL ao Parametro
        conexao.setDeclaracao("SELECT * FROM Logradouro WHERE  idLogradouro = ?");
        
        //Seta o parâmetro ? do select
        conexao.getDeclaracao().setInt(1, idLogradouro);
        
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        conexao.getResultado().last();
        
        return PopulaLogradouro(conexao.getResultado());
        
    }
    
    public Boolean Update(Logradouro logradouro) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeLogradouro FROM Logradouro SET (?) WHERE idLogradouro = ?");
        
        conexao.getDeclaracao().setString(1, logradouro.getNomeLogradouro());
        conexao.getDeclaracao().setInt(2, logradouro.getIdLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Logradouro logradouro) throws SQLException {
        
        conexao.setDeclaracao("REMOVE FROM Logradouro WHERE idLogradouro = ?");
        
        conexao.getDeclaracao().setInt(1, logradouro.getIdLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Logradouro> ListByTipoLogradouro(int idTipoLogradouro) throws SQLException{
    ArrayList<Logradouro> logradouros = new ArrayList<>();
    
    conexao.setDeclaracao("SELECT * " +
                            "FROM  `Logradouro` " +
                            "WHERE  `idTipoLogradouro` = ?");

    conexao.getDeclaracao().setInt(1,idTipoLogradouro);
    
    conexao.setResultado(conexao.getDeclaracao().executeQuery());
    
    while(conexao.getResultado().next()){
        Logradouro logradouro = new Logradouro();
        logradouro = PopulaLogradouro(conexao.getResultado());  
        logradouros.add(logradouro);
    }
    return logradouros;
    }
}
