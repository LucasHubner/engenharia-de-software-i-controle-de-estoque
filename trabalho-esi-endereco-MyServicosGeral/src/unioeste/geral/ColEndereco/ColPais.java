package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Pais;

/**
 *
 * @author lucas
 */
public class ColPais {
    
    private Conexao conexao;

    public ColPais(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Pais PopulaPais(ResultSet resultado) throws SQLException{
         
        Pais pais = new Pais();
        
        pais.setIdPais(resultado.getInt("idPais"));
        pais.setNomePais(resultado.getString("nomePais"));
        pais.setSiglaPais(resultado.getString("siglaPais"));
        
        return pais;
    }
    
    public Boolean Create (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("INSERT * INTO Pais VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, pais.getNomePais());
        conexao.getDeclaracao().setString(2, pais.getSiglaPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Pais Read (int idPais) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Pais WHERE idPais = ?");
        
        conexao.getDeclaracao().setInt(1, idPais);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
       
        return PopulaPais(conexao.getResultado());
    }
    
    public Boolean Update (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomePais,siglaPais FROM Pais SET (?,?) WHERE idPais = ?");
        
        conexao.getDeclaracao().setString(1, pais.getNomePais());
        conexao.getDeclaracao().setString(2, pais.getSiglaPais());
        conexao.getDeclaracao().setInt(3, pais.getIdPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Pais pais) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Pais WHERE idPais = ?");
        
        conexao.getDeclaracao().setInt(1, pais.getIdPais());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Pais> List() throws SQLException{
        ArrayList<Pais> paises = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Pais");
        
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            paises.add(PopulaPais(conexao.getResultado()));
        }
        
        return paises;
        
    }
}
