package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Bairro;

/**
 *
 * @author lucas
 */
public class ColBairro {
    
    private Conexao conexao;

    public ColBairro(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Bairro PopulaBairro(ResultSet resultado) throws SQLException{
        Bairro bairro = new Bairro();
        
        bairro.setIdBairro(resultado.getInt("idBairro"));
        bairro.setNomeBairro(resultado.getString("nomeBairro"));
        
        return bairro;
    }
    
    public Boolean Create(Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("INSERT INTO `Bairro` (`idBairro`, `nomeBairro`) VALUES (NULL, '"+bairro.getNomeBairro()+"')");
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Bairro Read (int idBairro) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Bairro WHERE idBairro = ?");
        
        conexao.getDeclaracao().setInt(1, idBairro);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
        
        return PopulaBairro(conexao.getResultado());
    }
    
    public Boolean Update (Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeBairro FROM Bairro SET (?) WHERE idBairro = ?");
        
        conexao.getDeclaracao().setString(1, bairro.getNomeBairro());
        conexao.getDeclaracao().setInt(2, bairro.getIdBairro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (Bairro bairro) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Bairro WHERE idBairro = ?");
        
        conexao.getDeclaracao().setInt(1, bairro.getIdBairro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Bairro> List() throws SQLException{
        ArrayList<Bairro> bairros = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Bairro");
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            Bairro bairro = PopulaBairro(conexao.getResultado());
            bairros.add(bairro);
        }
        
        return bairros;
    }
}
