package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.UnidadeFederativa;

/**
 *
 * @author lucas
 */
public class ColUnidadeFederativa {
    private Conexao conexao;

    public ColUnidadeFederativa(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private UnidadeFederativa PopulaUnidadeFederativa(ResultSet resultado) throws SQLException{
        UnidadeFederativa unidadeFederativa = new UnidadeFederativa();
        
        unidadeFederativa.setIdUnidadeFederativa(resultado.getInt("idUnidadeFederativa"));
        unidadeFederativa.setNomeUnidadeFederativa(resultado.getString("nomeUF"));
        unidadeFederativa.setSiglaUnidadeFederativa(resultado.getString("siglaUF"));
        
        return unidadeFederativa;
    }
    
    public Boolean Create (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("INSERT * INTO UnidadeFederativa VALUES (null,?,?)");
        
        conexao.getDeclaracao().setString(1, unidadeFederativa.getNomeUnidadeFederativa());
        conexao.getDeclaracao().setString(2, unidadeFederativa.getSiglaUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public UnidadeFederativa Read (int idUnidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM UnidadeFederativa WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setInt(1, idUnidadeFederativa);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
        
        return PopulaUnidadeFederativa(conexao.getResultado());
    }
    
    public Boolean Update (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("UPDATE nomeUnidadeFederativa,siglaUnidadeFederativa FROM UnidadeFederativa SET (?,?) WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setString(1, unidadeFederativa.getNomeUnidadeFederativa());
        conexao.getDeclaracao().setString(2, unidadeFederativa.getSiglaUnidadeFederativa());
        conexao.getDeclaracao().setInt(3, unidadeFederativa.getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove (UnidadeFederativa unidadeFederativa) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM UnidadeFederativa WHERE idUnidadeFederativa = ?");
        
        conexao.getDeclaracao().setInt(1, unidadeFederativa.getIdUnidadeFederativa());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<UnidadeFederativa> List() throws SQLException{
        ArrayList<UnidadeFederativa> ufs = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM UnidadeFederativa");
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            ufs.add(PopulaUnidadeFederativa(conexao.getResultado()));
        }
                
        return ufs;
    }
}
