package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.TipoLogradouro;

/**
 *
 * @author lucas
 */
public class ColTipoLogradouro {
    private Conexao conexao;

    public ColTipoLogradouro(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private TipoLogradouro PopulaTipoLogradouro(ResultSet resultado) throws SQLException{
        TipoLogradouro tipoLogradouro = new TipoLogradouro();
        
        tipoLogradouro.setIdTipoLogradouro(resultado.getInt("idTipoLogradouro"));
        tipoLogradouro.setNomeTipoLogradouro(resultado.getString("nomeTipoLogradouro"));
        tipoLogradouro.setSiglaTipoLogradouro(resultado.getString("siglaTipoLogradouro"));
        
        return tipoLogradouro;
    }
    
     public Boolean Create(TipoLogradouro tipoLogradouro) throws SQLException{
        try{
            
            conexao.setDeclaracao("INSERT * INTO TipoLogradouro VALUES (null, ?,?)");
         
            conexao.getDeclaracao().setString(1, tipoLogradouro.getNomeTipoLogradouro());
            conexao.getDeclaracao().setString(2, tipoLogradouro.getSiglaTipoLogradouro());
            conexao.getDeclaracao().executeUpdate();
            
        }
        catch(SQLException exception){
            System.out.println(exception);
        }
        return false;
    }
    public TipoLogradouro Read(int idTipoLogradouro) throws SQLException  {
        
        conexao.setDeclaracao("SELECT * FROM TipoLogradouro WHERE  idTipoLogradouro = ?");
        conexao.getDeclaracao().setInt(1, idTipoLogradouro);
      
        conexao.executeQueryAndSetDeclaracao();
        conexao.getResultado().last();
        
        //Seta o ID do objeto tipologradouro para a ID recebida pelo query
        // Observe que se usa o conexao.getResultado().getInt("NOME DO ELEMENTO DA TABELA")
        
        return PopulaTipoLogradouro(conexao.getResultado());
    }
    
    public Boolean Update(TipoLogradouro tipoLogradouro) throws SQLException{
        
        conexao.setDeclaracao("UPDATE nomeTipoLogradouro,siglaTipoLogradouro FROM TipoLogradouro SET (?,?) WHERE idTipoLogradouro = ?");
        
        conexao.getDeclaracao().setString(1, tipoLogradouro.getNomeTipoLogradouro());
        conexao.getDeclaracao().setString(2, tipoLogradouro.getSiglaTipoLogradouro());
        conexao.getDeclaracao().setInt(3, tipoLogradouro.getIdTipoLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(TipoLogradouro tipoLogradouro) throws SQLException{
        
        conexao.setDeclaracao("DELETE FROM TipoLogradouro WHERE idTipoLogradouro = ?");
        
        conexao.getDeclaracao().setInt(1, tipoLogradouro.getIdTipoLogradouro());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<TipoLogradouro> List() throws SQLException{
        ArrayList<TipoLogradouro> tiposLogradouro = new ArrayList<>();
        
        conexao.setDeclaracao("SELECT * FROM TipoLogradouro");
        conexao.executeQueryAndSetDeclaracao();
        
        while(conexao.getResultado().next()){
            TipoLogradouro tipoLogradouro = new TipoLogradouro();
            tipoLogradouro = PopulaTipoLogradouro(conexao.getResultado());
            tiposLogradouro.add(tipoLogradouro);
        }
        
        return tiposLogradouro;
    }
}
