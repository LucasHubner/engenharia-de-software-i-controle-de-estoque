package unioeste.geral.ColEndereco;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Endereco;

/**
 *
 * @author lucas
 */
public class ColEndereco {
    
    private Conexao conexao;

    public ColEndereco(Conexao conexao) {
        this.conexao = conexao;
    }
    
    
    private Endereco PopulaEndereco(ResultSet resultado) throws SQLException{
        Endereco endereco = new Endereco();
        Cep cep = new Cep();
        ColCidade daoEnderecoCidade = new ColCidade(conexao);
        ColBairro daoEnderecoBairro = new ColBairro(conexao);
        ColLogradouro daoEnderecoLogradouro = new ColLogradouro(conexao);
        ColPais daoEnderecoPais = new ColPais(conexao);
        
        endereco.setIdEndereco(resultado.getInt("idEndereco"));
        cep.setCepInt(resultado.getInt("cep"));
        endereco.setCep(cep);
        endereco.setCidade(daoEnderecoCidade.Read(resultado.getInt("idCidade")));
        endereco.setBairro(daoEnderecoBairro.Read(resultado.getInt("idBairro")));
        endereco.setLogradouro(daoEnderecoLogradouro.Read(resultado.getInt("idRua")));
        endereco.setPais(daoEnderecoPais.Read(resultado.getInt("idPais")));
        
        return endereco;
    }
    public Endereco Create (Endereco endereco) throws SQLException{
        
        try {
            conexao.setDeclaracao("INSERT INTO `Endereco`(`idEndereco`, `cep`, `idRua`, `idBairro`, `idPais`, `idCidade`)"
                    + " VALUES (null,?,?,?,?,?)");
        
            conexao.getDeclaracao().setInt(1, endereco.getCep().getCepInt());
            conexao.getDeclaracao().setInt(2, endereco.getLogradouro().getIdLogradouro());
            conexao.getDeclaracao().setInt(3, endereco.getBairro().getIdBairro());
            conexao.getDeclaracao().setInt(4, endereco.getPais().getIdPais());
            conexao.getDeclaracao().setInt(5, endereco.getCidade().getIdCidade());

            endereco.setIdEndereco(conexao.getDeclaracao().executeUpdate());

            
        } catch (Exception ex) {
            Logger.getLogger(ColEndereco.class.getName()).log(Level.SEVERE, null, ex);
            
            System.out.println(ex.getMessage());
            throw ex;
        }
        
        return endereco;
    }
    
    public Endereco Read (int idEndereco) throws SQLException {
        
        conexao.setDeclaracao("SELECT * FROM Endereco WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, idEndereco);
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        conexao.getResultado().last();
        
        
        return PopulaEndereco(conexao.getResultado());
    }
    
    public Boolean Update(Endereco endereco) throws SQLException {
        
        conexao.setDeclaracao("UPDATE cep FROM Endereco SET (?) WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, endereco.getCep().getCepInt());
        conexao.getDeclaracao().setInt(2, endereco.getIdEndereco());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public Boolean Remove(Endereco endereco) throws SQLException {
        
        conexao.setDeclaracao("DELETE FROM Endereco WHERE idEndereco = ?");
        
        conexao.getDeclaracao().setInt(1, endereco.getIdEndereco());
        conexao.getDeclaracao().executeUpdate();
        
        return true;
    }
    
    public ArrayList<Endereco> SearchByCep(Cep cep) throws SQLException{
        ArrayList<Endereco> enderecos = new ArrayList<>();
        
        conexao.setDeclaracao("SELECT * FROM Endereco WHERE cep = ?");
        
        conexao.getDeclaracao().setInt(1, cep.getCepInt());
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            enderecos.add(PopulaEndereco(conexao.getResultado()));
        }
        
        
        return enderecos;
    }
    
}
