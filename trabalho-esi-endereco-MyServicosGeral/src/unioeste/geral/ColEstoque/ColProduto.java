/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.ColEstoque;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.bo.estoque.Produto;

/**
 *
 * @author lucas
 */
public class ColProduto {
    private Conexao conexao;
    
    public ColProduto(Conexao conexao){
        this.conexao = conexao;
    }
    
    private Produto PopulaProduto(ResultSet resultado) throws SQLException{
        Produto produto = new Produto();
        produto.setCodBarras(resultado.getInt("codBarras"));
        produto.setNomeProduto(resultado.getString("nomeProduto"));
        produto.setPrecoCuso(resultado.getFloat("precoCusto"));
        produto.setPrecoVenda(resultado.getFloat("precoVenda"));
        produto.setQtdMinima(resultado.getInt("qtdMinima"));
        produto.setQtdEstoque(resultado.getInt("qtdEstoque"));
        
        
        return produto;
    }
    
    public Produto Create(Produto produto) throws SQLException{
        conexao.setDeclaracao("INSERT INTO `Produto`(`codBarras`, `nomeProduto`, `precoCusto`, `precoVenda`, `qtdMinima`,`qtdEstoque`)"
                + "  VALUES (?,?,?,?,?,?)");
        
        conexao.getDeclaracao().setInt(1,produto.getCodBarras());
        conexao.getDeclaracao().setString(2, produto.getNomeProduto());
        conexao.getDeclaracao().setFloat(3, produto.getPrecoCuso());
        conexao.getDeclaracao().setFloat(4, produto.getPrecoVenda());
        conexao.getDeclaracao().setInt(5, produto.getQtdMinima());
        conexao.getDeclaracao().setInt(6, produto.getQtdEstoque());
        conexao.getDeclaracao().executeUpdate();
        
        return produto;
    }
    
    public boolean UpdateEstoque(Produto produto,int estoque){
        try{
            conexao.setDeclaracao("UPDATE `Produto` SET `qtdEstoque` = `qtdEstoque` + ? WHERE codBarras = ? ");
            conexao.getDeclaracao().setInt(1,produto.getCodBarras());
            conexao.getDeclaracao().setInt(2,estoque);
            
            conexao.getDeclaracao().executeUpdate();
            return true;
        }
        catch(Exception ex){
            return false;
        }
    }
    
    public ArrayList<Produto> List() throws SQLException{
        ArrayList<Produto> produtos = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Produto");
        conexao.setResultado(conexao.getDeclaracao().executeQuery());
        
        while(conexao.getResultado().next()){
            produtos.add(PopulaProduto(conexao.getResultado()));
        }
        
        return produtos;
    }
    
}
