/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.ColCompra;

import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEstoque.ColProduto;
import unioeste.geral.bo.estoque.Compra;
import unioeste.geral.bo.estoque.ProdutoTransacao;

/**
 *
 * @author lucas
 */
public class ColCompra {
    Conexao conexao;

    public ColCompra(Conexao conexao) {
        this.conexao = conexao;
    }
    
    public Compra Create(Compra compra) throws SQLException {
        try{
            conexao.setDeclaracao("INSERT INTO  "
                    + "`Compra` (  `idCompra` ,  `dataCompra` ,  `cnpjFornecedor` )" +
                    "VALUES (" +
                    "NULL , NULL , ?" +
                    ")");

//            conexao.getDeclaracao().setDate(1, (Date) compra.getData());
            conexao.getDeclaracao().setString(1, compra.getFornecedor().getCnpj().getCnpjString());

            for(ProdutoTransacao transacao:compra.getProdutos()){
                ColProduto col = new ColProduto(conexao);
                col.UpdateEstoque(transacao.getProduto(), transacao.getQtProduto());
            }

            compra.setId(conexao.getDeclaracao().executeUpdate());
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            throw ex;
        }
        
        return compra;
    }
    
}
