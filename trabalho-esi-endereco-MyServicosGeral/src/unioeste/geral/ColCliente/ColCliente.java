/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.geral.ColCliente;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import unioeste.apoio.BD.Conexao;
import unioeste.geral.ColEndereco.ColEndereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.CPF;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.NomePessoa;

/**
 *
 * @author lucas
 */
public class ColCliente {
    private Conexao conexao;
    public ColCliente(Conexao conexao) {
        this.conexao = conexao;
    }
    
    private Cliente PopulaCliente(ResultSet resultado) throws SQLException{
        Cliente cliente = new Cliente();
        //ID
        cliente.setIdCliente(resultado.getInt("idCliente"));

        //Nome
        NomePessoa nome = new NomePessoa();
        nome.setNomeCompleto(resultado.getString("nomeCliente"));
        cliente.setNome(nome);
        
        //CPF
        CPF cpf = new CPF();
        cpf.setCpfString(resultado.getString("cpfCliente"));
        cliente.setCpf(cpf);
        
        //CNPJ
        CNPJ cnpj = new CNPJ();
        cnpj.setCnpjString(resultado.getString("cnpjCliente"));
        cliente.setCnpj(cnpj);
        
        //Endereco Complemento
        EnderecoEspecifico enderecoEsp = new EnderecoEspecifico();
        enderecoEsp.setComplementoEndereco(resultado.getString("enderecoComplemento"));
        enderecoEsp.setNroEndereco(resultado.getInt("enderecoNumero"));
        ColEndereco colEndereco = new ColEndereco(conexao);
        enderecoEsp.setEndereco(colEndereco.Read(resultado.getInt("idEndereco")));
        cliente.setEnderecoPrincipal(enderecoEsp);
        
                
        return cliente;
    }
    
    public Cliente Create(Cliente cliente) throws SQLException{
        conexao.setDeclaracao("INSERT INTO `Cliente`(`idCliente`, "
                                                + "`nomeCliente`, "
                                                + "`cpfCliente`, "
                                                + "`cnpjCliente`, "
                                                + "`enderecoComplemento`, "
                                                + "`enderecoNumero`, "
                                                + "`idEndereco`)"
                + " VALUES (null,?,?,?,?,?,?)");
        
        conexao.getDeclaracao().setString(1, cliente.getNome().getNomeCompleto());
        if(cliente.getCpf() != null){
            conexao.getDeclaracao().setString(2,cliente.getCpf().getCpfString());
            conexao.getDeclaracao().setString(3,null);
        }
        else{
            conexao.getDeclaracao().setString(2,null);
            conexao.getDeclaracao().setString(3,cliente.getCnpj().getCnpjString());
        }
        conexao.getDeclaracao().setString(4,cliente.getEnderecoPrincipal().getComplementoEndereco());
        conexao.getDeclaracao().setInt(5,cliente.getEnderecoPrincipal().getNroEndereco());
        conexao.getDeclaracao().setInt(6,cliente.getEnderecoPrincipal().getEndereco().getIdEndereco());
        
        conexao.getDeclaracao().executeUpdate();
        
        return cliente;
    }
    
    public ArrayList<Cliente> List() throws SQLException{
        ArrayList<Cliente> clientes = new ArrayList<>();
        conexao.setDeclaracao("SELECT * FROM Cliente");
        conexao.executeQueryAndSetDeclaracao();
        while(conexao.getResultado().next()){
            clientes.add(PopulaCliente(conexao.getResultado()));
        }
        
        return clientes;
    }
}
