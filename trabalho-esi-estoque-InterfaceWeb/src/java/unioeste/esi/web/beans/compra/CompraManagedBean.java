/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.esi.web.beans.compra;

import java.io.IOException;
import java.util.ArrayList;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import java.util.Date;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.estoque.Compra;
import unioeste.geral.bo.estoque.Produto;
import unioeste.geral.bo.estoque.ProdutoTransacao;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.manager.UCCompraGeralServicos;
/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class CompraManagedBean {
    private Date data;
    private String cnpjFornecedor;
    private Integer codBarras;
    private Integer qtProduto;
    private Float precoCompra;
    private UCCompraGeralServicos service = new UCCompraGeralServicos();
    /**
     * Creates a new instance of CompraManagedBean
     */
    public CompraManagedBean() {
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getCnpjFornecedor() {
        return cnpjFornecedor;
    }

    public void setCnpjFornecedor(String cnpjFornecedor) {
        this.cnpjFornecedor = cnpjFornecedor;
    }

    public Integer getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(Integer codBarras) {
        this.codBarras = codBarras;
    }

    public Integer getQtProduto() {
        return qtProduto;
    }

    public void setQtProduto(Integer qtProduto) {
        this.qtProduto = qtProduto;
    }

    public Float getPrecoCompra() {
        return precoCompra;
    }

    public void setPrecoCompra(Float precoCompra) {
        this.precoCompra = precoCompra;
    }
    
    public void onSubmit() throws Exception{
        Compra compra = new Compra();
        compra.setData(data);
        Fornecedor fornecedor = new Fornecedor();
        CNPJ cnpj = new CNPJ();
        cnpj.setCnpjString(cnpjFornecedor);
        fornecedor.setCnpj(cnpj);
        compra.setFornecedor(fornecedor);
        
        //Cria objeto de transação
        ArrayList<ProdutoTransacao> produtos = new ArrayList<>();
        //Cria o produto que vai dentro do objeto de transacao
        Produto produto = new Produto();
        produto.setCodBarras(codBarras);
        //Isso aqui td vai dentro do objeto transacao
        ProdutoTransacao produtoTransacao = new ProdutoTransacao();
        produtoTransacao.setProduto(produto);
        produtoTransacao.setQtProduto(qtProduto);
        produtoTransacao.setPrecoTransacao(precoCompra);
        produtos.add(produtoTransacao);
        
        compra.setProdutos(produtos);
        service.CadastraCompra(compra);
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
        
    }
    
    
}
