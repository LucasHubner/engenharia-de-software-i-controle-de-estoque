/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.esi.web.beans.produto;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.estoque.Produto;
import unioeste.geral.manager.UCProdutoGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class ProdutoManagedBean {
    private Integer codBarras;
    private String nomeProduto = new String();
    private String precoCusto;
    private String precoVenda;
    private Integer qtdMinima;
    private Integer qtdEstoque;
    private UCProdutoGeralServicos service = new UCProdutoGeralServicos();

    private ArrayList<Produto> produtos;
            
    /**
     * Creates a new instance of ProdutoManagedBean
     */
    public ProdutoManagedBean(){
        try  {
            produtos = service.ListaProdutos();
        } catch (SQLException ex) {
            Logger.getLogger(ProdutoManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Integer getQtdEstoque() {
        return qtdEstoque;
    }

    public void setQtdEstoque(Integer qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }
    
    public Integer getCodBarras(){
        return codBarras;
    }

    public void setCodBarras(Integer codBarras) {
        this.codBarras = codBarras;
    }

    public String getNomeProduto() {
        return nomeProduto;
    }

    public void setNomeProduto(String nomeProduto) {
        this.nomeProduto = nomeProduto;
    }

    public String getPrecoCusto() {
        return precoCusto;
    }

    public void setPrecoCusto(String precoCusto) {
        this.precoCusto = precoCusto;
    }

    public String getPrecoVenda() {
        return precoVenda;
    }

    public void setPrecoVenda(String precoVenda) {
        this.precoVenda = precoVenda;
    }

    public Integer getQtdMinima() {
        return qtdMinima;
    }

    public void setQtdMinima(Integer qtdMinima) {
        this.qtdMinima = qtdMinima;
    }
    public void onSubmit() throws SQLException{
        Produto produto = new Produto();
        
        produto.setCodBarras(codBarras);
        produto.setNomeProduto(nomeProduto);
        produto.setPrecoCuso(Float.parseFloat(precoCusto));
        produto.setPrecoVenda(Float.parseFloat(precoVenda));
        produto.setQtdMinima(qtdMinima);
        produto.setQtdEstoque(qtdEstoque);
        
        service.CadastraProduto(produto);
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
    }
    
    
}
