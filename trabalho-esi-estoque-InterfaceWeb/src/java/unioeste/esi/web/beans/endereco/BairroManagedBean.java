package unioeste.esi.web.beans.endereco;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.manager.UCEnderecoGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean(name = "bairroManagedBean")
@RequestScoped
public class BairroManagedBean {
    String nomeBairro = new String();
    ArrayList<Bairro> bairros;
    UCEnderecoGeralServicos enderecoService = new UCEnderecoGeralServicos();
            
    /**
     * Creates a new instance of BairroManagedBean
     */
    public BairroManagedBean() {
        bairros = enderecoService.listBairrosArrayList();
    }

    public ArrayList<Bairro> getBairros() {
        return bairros;
    }

    public void setBairros(ArrayList<Bairro> bairros) {
        this.bairros = bairros;
    }

    public String getNomeBairro() {
        return nomeBairro;
    }

    public void setNomeBairro(String nomeBairro) {
        this.nomeBairro = nomeBairro;
    }
    
    public void onSubmit() throws IOException{
        try {
            Bairro bairro = new Bairro();
            bairro.setNomeBairro(this.nomeBairro);
            enderecoService.cadastrarBairro(bairro);
             
        } catch (SQLException ex) {
            Logger.getLogger(BairroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
    }
}
