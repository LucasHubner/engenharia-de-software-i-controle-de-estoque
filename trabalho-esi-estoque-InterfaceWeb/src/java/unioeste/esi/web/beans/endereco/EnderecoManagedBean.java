package unioeste.esi.web.beans.endereco;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.esi.web.beans.cliente.ClienteManagedBean;
import unioeste.geral.bo.endereco.Bairro;
import unioeste.geral.bo.endereco.Cep;
import unioeste.geral.bo.endereco.Cidade;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.Pais;
import unioeste.geral.bo.endereco.UnidadeFederativa;
import unioeste.geral.manager.UCEnderecoGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class EnderecoManagedBean {
    UCEnderecoGeralServicos enderecoService = new UCEnderecoGeralServicos();

    //Atributos a serem usados pela classe para query
    //Estes atributos são utilizados pelo framework PrimeFaces
    //Para fazer o controle do Ajax
    private Integer cep;
    private Integer pais;
    private Integer estado;
    private Integer cidade;
    private Integer tipoLogradouro;
    private Integer bairro;
    private Integer logradouro;
    
    
    //Listas que populam os selectBox
    private Map<String, Integer> paises;
    private Map<String, Integer> ufs;
    private Map<String, Integer> cidades;
    private Map<String, Integer> tiposLogradouro;
    private Map<String, Integer> bairros;
    private Map<String, Integer> logradouros;
    
    private int size;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
    
    
    private ArrayList<Logradouro> logradourosArrayList;

    public EnderecoManagedBean() {
        
    }

    @PostConstruct
    public void init(){
        logradourosArrayList = new ArrayList<>();
        logradouros = new HashMap<String, Integer>();
        cidades = new HashMap<String, Integer>();
        tiposLogradouro =  enderecoService.listTiposLogradouro();
        ufs = enderecoService.listEstados();
        paises = enderecoService.listPaises();
        bairros = enderecoService.listBairros();
        logradouros = enderecoService.listLogradouros(17);
    }

    public ArrayList<Logradouro> getLogradourosArrayList() {
        return logradourosArrayList;
    }

    public void setLogradourosArrayList(ArrayList<Logradouro> logradourosArrayList) {
        this.logradourosArrayList = logradourosArrayList;
    }

    public Integer getCep() {
        return cep;
    }

    public void setCep(Integer cep) {
        this.cep = cep;
    }

    public Integer getBairro() {
        return bairro;
    }

    public void setBairro(Integer bairro) {
        this.bairro = bairro;
    }

    public Integer getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(Integer logradouro) {
        this.logradouro = logradouro;
    }

    public Map<String, Integer> getBairros() {
        return bairros;
    }

    public void setBairros(Map<String, Integer> bairros) {
        this.bairros = bairros;
    }

    public Map<String, Integer> getLogradouros() {
        return logradouros;
    }

    public void setLogradouros(Map<String, Integer> logradouros) {
        this.logradouros = logradouros;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public Integer getTipoLogradouro() {
        return tipoLogradouro;
    }

    public void setTipoLogradouro(Integer tipoLogradouro) {
        this.tipoLogradouro = tipoLogradouro;
    }
 
    public Map<String, Integer> getUfs() {
        return ufs;
    }
 
    public Map<String, Integer> getCities() {
        return cidades;
    }
    public Map<String, Integer> getEstados(){
        return ufs;
    }

    public Integer getCidade() {
        return cidade;
    }

    public void setCidade(Integer cidade) {
        this.cidade = cidade;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Map<String, Integer> getPaises() {
        return paises;
    }

    public void setPaises(Map<String, Integer> paises) {
        this.paises = paises;
    }

    public Map<String, Integer> getCidades() {
        return cidades;
    }

    public void setCidades(Map<String, Integer> cidades) {
        this.cidades = cidades;
    }

    public Map<String, Integer> getTiposLogradouro() {
        return tiposLogradouro;
    }

    public void setTiposLogradouro(Map<String, Integer> tiposLogradouro) {
        this.tiposLogradouro = tiposLogradouro;
    }
 
    
    public void onUnidadeFederativaChange() throws SQLException {
        UnidadeFederativa uf = new UnidadeFederativa();
        uf.setIdUnidadeFederativa(estado);
        this.cidades = enderecoService.listCidades(uf);
        
    }
    
    public void onTipoLogradouroChange(){
        logradouros = enderecoService.listLogradouros(tipoLogradouro);
        logradourosArrayList = enderecoService.listLogradourosArrayList(tipoLogradouro);
        size = logradourosArrayList.size();
        
    }
    
    public void onSubmit() throws SQLException, IOException{
        try{
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        
            Endereco endereco = new Endereco();
            Cep cepEndereco = new Cep();
            cepEndereco.setCepInt(this.cep);
            endereco.setCep(cepEndereco);

            Pais paisEndereco = new Pais();
            paisEndereco.setIdPais(this.pais);
            endereco.setPais(paisEndereco);

            Cidade cidade = new Cidade();
            cidade.setIdCidade(this.cidade);
            endereco.setCidade(cidade);

            Logradouro logradouro = new Logradouro();
            logradouro.setIdLogradouro(this.logradouro);
            endereco.setLogradouro(logradouro);

            Bairro bairro = new Bairro();
            bairro.setIdBairro(this.bairro);
            endereco.setBairro(bairro);

            enderecoService.cadastrarEndereco(endereco);

            context.redirect(context.getRequestContextPath()+"/");
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
            throw ex ;
        }
        }
        
}
