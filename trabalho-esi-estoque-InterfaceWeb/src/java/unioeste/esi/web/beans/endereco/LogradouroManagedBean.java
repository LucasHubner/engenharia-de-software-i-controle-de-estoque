package unioeste.esi.web.beans.endereco;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.endereco.Logradouro;
import unioeste.geral.bo.endereco.TipoLogradouro;
import unioeste.geral.manager.UCEnderecoGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class LogradouroManagedBean {
    Integer idTipoLogradouro;
    String nomeLogradouro = new String();
    UCEnderecoGeralServicos enderecoService = new UCEnderecoGeralServicos();

    /**
     * Creates a new instance of LogradouroManagedBean
     */
    public LogradouroManagedBean() {
    }

    public Integer getIdTipoLogradouro() {
        return idTipoLogradouro;
    }

    public void setIdTipoLogradouro(Integer idTipoLogradouro) {
        this.idTipoLogradouro = idTipoLogradouro;
    }

    public String getNomeLogradouro() {
        return nomeLogradouro;
    }

    public void setNomeLogradouro(String nomeLogradouro) {
        this.nomeLogradouro = nomeLogradouro;
    }
    
    public void onSubmit(){
        try {
            Logradouro logradouro = new Logradouro();
            TipoLogradouro tipoLogradouro = new TipoLogradouro();
            tipoLogradouro.setIdTipoLogradouro(this.idTipoLogradouro);
            logradouro.setTipoLogradouro(tipoLogradouro);
            logradouro.setNomeLogradouro(this.nomeLogradouro);
            enderecoService.cadastraLogradouro(logradouro);
        } catch (SQLException ex) {
            Logger.getLogger(LogradouroManagedBean.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print(this.idTipoLogradouro);
            System.out.print(this.nomeLogradouro);
        }
            ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
    }
}
