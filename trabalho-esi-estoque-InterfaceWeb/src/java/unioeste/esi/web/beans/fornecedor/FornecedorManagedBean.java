/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.esi.web.beans.fornecedor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.Fornecedor;
import unioeste.geral.bo.pessoa.NomePessoa;
import unioeste.geral.manager.UCFornecedorGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class FornecedorManagedBean {
    
    private String cnpj;
    private String complemento;
    private String nomeFornecedor;
    private Integer numeroEndereco;
    private Integer idEndereco;
    private UCFornecedorGeralServicos service = new UCFornecedorGeralServicos();
    private ArrayList<Fornecedor> fornecedores;
    /**
     * Creates a new instance of FornecedorManagedBean
     */
    public FornecedorManagedBean() {
        try {
            fornecedores = service.List();
        } catch (SQLException ex) {
            Logger.getLogger(FornecedorManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getNomeFornecedor() {
        return nomeFornecedor;
    }

    public void setNomeFornecedor(String nomeFornecedor) {
        this.nomeFornecedor = nomeFornecedor;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }


    public Integer getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(Integer numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public Integer getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(Integer idEndereco) {
        this.idEndereco = idEndereco;
    }

    public ArrayList<Fornecedor> getFornecedores() {
        return fornecedores;
    }

    public void setFornecedores(ArrayList<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }
     public void onSubmit() throws SQLException{
        Fornecedor fornecedor = new Fornecedor();
        
        NomePessoa nome = new NomePessoa();
        nome.setNomeCompleto(nomeFornecedor);
        fornecedor.setNome(nome);
        
        EnderecoEspecifico endereco = new EnderecoEspecifico();
        endereco.setNroEndereco(this.numeroEndereco);
        endereco.setComplementoEndereco(this.complemento);
        
        Endereco enderecoCliente = new Endereco();
        enderecoCliente.setIdEndereco(idEndereco);
        endereco.setEndereco(enderecoCliente);

        fornecedor.setEnderecoPrincipal(endereco);

        CNPJ cnpjFornecedor = new CNPJ();
        cnpjFornecedor.setCnpjString(cnpj);
        fornecedor.setCnpj(cnpjFornecedor);

        service.CadastraFornecedor(fornecedor);
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
    }
}
