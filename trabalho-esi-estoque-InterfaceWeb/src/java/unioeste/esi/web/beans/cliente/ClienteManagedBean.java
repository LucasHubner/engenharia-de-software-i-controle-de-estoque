/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.esi.web.beans.cliente;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import unioeste.geral.bo.endereco.Endereco;
import unioeste.geral.bo.pessoa.CNPJ;
import unioeste.geral.bo.pessoa.CPF;
import unioeste.geral.bo.pessoa.Cliente;
import unioeste.geral.bo.pessoa.EnderecoEspecifico;
import unioeste.geral.bo.pessoa.NomePessoa;
import unioeste.geral.bo.pessoa.Pessoa;
import unioeste.geral.bo.pessoa.PessoaFisica;
import unioeste.geral.bo.pessoa.PessoaJuridica;
import unioeste.geral.manager.UCClienteGeralServicos;

/**
 *
 * @author lucas
 */
@ManagedBean
@RequestScoped
public class ClienteManagedBean {
    
    private String cpf;
    private String cnpj;
    private String complemento;
    private String nomeCliente;
    private Integer numeroEndereco;
    private Integer idEndereco;
    private String tipoDocumento;
    private ArrayList<Cliente> clientes;
    
    private UCClienteGeralServicos service = new UCClienteGeralServicos();
    /**
     * Creates a new instance of ClienteManagedBean
     */
    
    public ClienteManagedBean() {
        tipoDocumento = "CPF";
        try {
            clientes = service.ListaClientes();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteManagedBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(ArrayList<Cliente> clientes) {
        this.clientes = clientes;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public Integer getNumeroEndereco() {
        return numeroEndereco;
    }

    public void setNumeroEndereco(Integer numeroEndereco) {
        this.numeroEndereco = numeroEndereco;
    }

    public Integer getIdEndereco() {
        return idEndereco;
    }

    public void setIdEndereco(Integer idEndereco) {
        this.idEndereco = idEndereco;
    }
    
    public void onSubmit(){
        Cliente cliente = new Cliente();
        
        NomePessoa nome = new NomePessoa();
        nome.setNomeCompleto(nomeCliente);
        cliente.setNome(nome);
        
        EnderecoEspecifico endereco = new EnderecoEspecifico();
        endereco.setNroEndereco(this.numeroEndereco);
        endereco.setComplementoEndereco(this.complemento);
        
            Endereco enderecoCliente = new Endereco();
            enderecoCliente.setIdEndereco(idEndereco);
            endereco.setEndereco(enderecoCliente);

        cliente.setEnderecoPrincipal(endereco);

        if(tipoDocumento.equals("CPF")){
            CPF cpfCliente = new CPF();
            cpfCliente.setCpfString(this.cpf);
            cliente.setCpf(cpfCliente);
        }
        else{
            CNPJ cnpjCliente = new CNPJ();
            cnpjCliente.setCnpjString(cnpj);
            cliente.setCnpj(cnpjCliente);
        }
        
        try {
            service.CadastrarCliente(cliente);
        } catch (Exception ex) {
            Logger.getLogger(ClienteManagedBean.class.getName()).log(Level.SEVERE, null, ex);
            
            FacesContext.getCurrentInstance().addMessage(
                    null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            ex.getMessage(), "title"));

            FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds()
            .add("globalMessage");

        }
        
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try { 
             context.redirect(context.getRequestContextPath()+"/");
        } catch (IOException e) { e.printStackTrace(); }
    }
}
