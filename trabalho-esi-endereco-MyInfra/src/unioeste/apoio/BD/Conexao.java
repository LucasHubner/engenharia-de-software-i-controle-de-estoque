/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unioeste.apoio.BD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author lucas
 */
public class Conexao {
    private static Connection conexao;
    private PreparedStatement declaracao;
    private ResultSet resultado;
    private String database = "mydb";
    
    public boolean AbreConexao(){
        try{
            Class.forName("com.mysql.jdbc.Driver");  
            String url = "jdbc:mysql://localhost:3306/" + database;
            String usuario = "root";
            String senha = "";
            conexao = DriverManager.getConnection(url,usuario,senha);
            return true;
        }
        catch(ClassNotFoundException | SQLException e){
            System.out.print(e);
            return false;
        }
    }
    
    public boolean FechaConexao(){
        try{
            conexao.close();
            return true;
        }
        catch(SQLException e){
            System.out.print(e);
            return false;
        }
    }

    public static Connection getConexao() {
        return conexao;
    }

    public static void setConexao(Connection conexao) {
        Conexao.conexao = conexao;
    }

    public PreparedStatement getDeclaracao() {
        return declaracao;
    }

    public void setDeclaracao(String declaracao) throws SQLException {
        this.declaracao = this.conexao.prepareStatement(declaracao);
        
    }

    public ResultSet getResultado() {
        return resultado;
    }

    public void setResultado(ResultSet resultado) {
        this.resultado = resultado;
    }
    
    public void executeQueryAndSetDeclaracao() throws SQLException{
        this.resultado = declaracao.executeQuery();
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }
}
